var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
    anhbaiviet: {type: String, required: true},
    tieude: {type: String, required: true},
    tomtat: {type: String, required: true},
    noidung: {type: String, required: true},
    thoigian: {type: Date, required: true},
    danhmuc_id: {type: Object, required: true},
    tacgia: {type: String, required: true},        
});
module.exports= mongoose.model('baiviet',schema);