var Baiviet = require('../models/baiviet');
var mongoose = require('mongoose');

mongoose.connect('localhost:27017/tintuc');
var baiviet = [
    new Baiviet({
        anhbaiviet:'images/tg1.png',
        tieude: 'Nhật Bản muốn mua tên lửa Tomahawk để đối phó Triều Tiên',
        tomtat: 'Một nguồn tin chính phủ Nhật Bản cho biết Tokyo đang cân nhắc việc sử dụng',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),  
    new Baiviet({
        anhbaiviet:'images/bai1.png',
        tieude: '11 người chết, 33 người bị thương sau khi ôtô tải đối đầu xe khách',
        tomtat: 'Rạng sáng nay, ôtô tải đầu kéo chở phân bón từ Gia Lai đi Đắk Lắk, khi đến thị trấn Chư Sê, huyện Chư Sê (Gia Lai), đã va chạm phải xe khách chạy ngược chiều, khiến 10 người chết.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai2.png',
        tieude: 'Chân dung Vietstar Airlines, hãng hàng không nội địa mới chờ cấp phép',
        tomtat: 'Trình lại phương án cấp phép vận chuyển hàng không, Vietstar Airlines điều chỉnh kế hoạch chỉ còn 10 máy bay nay đến 2020, trong đó 5 chiếc đậu ở Tân Sơn Nhất.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai3.png',
        tieude: 'Cụ ông 85 tuổi qua đời trong lúc chinh phục Everest',
        tomtat: 'Cụ ông 85 tuổi từ Nepal với mong muốn trở thành người cao tuổi nhất chinh phục "nóc nhà thế giới" đã qua đời tại khu trại ở độ cao hơn 5.000 m thuộc dãy Himalaya.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai4.png',
        tieude: 'Bị phản ứng, Vietcombank lùi hạn áp dụng quy định bảo mật mới',
        tomtat: 'Ngân hàng này đã phát đi thông báo chưa áp dụng văn bản quy định các điều kiện, điều khoản sử dụng dịch vụ ngân hàng điện tử, và cam kết sẽ có sửa đổi phù hợp.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai5.png',
        tieude: 'Cuốn sách dành cho những trái tim từng bị tổn thương',
        tomtat: 'Tiểu thuyết "Gã cướp biển quý tộc" viết về 2 trái tim từng bị tổn thương, một lần nữa tìm thấy tình yêu đích thực của mình.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai6.png',
        tieude: 'Nhà thơ Việt Phương qua đời ở tuổi 89',
        tomtat: 'Tác giả của tập thơ "Cửa mở" đình đám năm 1970 mới qua đời vào sáng 6/5 tại nhà riêng ở Hà Nội.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai7.png',
        tieude: 'Dàn hoa hậu, á hậu Hong Kong mặc gợi cảm dự sự kiện từ thiện',
        tomtat: 'Xuất hiện trong tiệc từ thiện được tổ chức vào tối 6/5 tại Hong Kong, nhiều hoa hậu, á hậu Hong Kong gây bất ngờ khi chọn lựa trang phục gợi cảm.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai8.png',
        tieude: 'Sao nữ Hàn rời Trung Quốc, trở về nhà khi mặt biến dạng',
        tomtat: 'Nữ diễn viên "Tình dục là chuyện nhỏ" Ham So Won chia tay bạn trai giàu có người Trung Quốc sau ba năm hẹn hò. Cũng lúc này, cô đối diện với nhiều chỉ trích khi gương mặt thay đổi.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),
    new Baiviet({
        anhbaiviet:'images/bai9.png',
        tieude: 'Loạt sao phải lòng áo tắm nude cho hè 2017',
        tomtat: 'Tông màu sang trọng này năm nay dự báo sẽ xuất hiện rất nhiều trên các bãi biển.',
        noidung: '',
        thoigian: '',
        tacgia: '',
        danhmuc_id:[
            {
                tendanhmuc: 'thegioi',
            },
            {
                tendanhmuc: 'kinhdoanh',
            },
             {
                tendanhmuc: 'phapluat',
            },
             {
                tendanhmuc: 'thethao',
            },
        ]

    }),      

];
var done = 0;
for (var i = 0; i < baiviet.length; i++) {
    baiviet[i].save(function (err, result) {
        done++;
        if (done == baiviet.length) {
            exit();
        }
    });
};

function exit() {
    mongoose.disconnect();
}