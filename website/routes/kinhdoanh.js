var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('kinhdoanh', { title: 'Kinh Doanh' });
});

module.exports = router;
