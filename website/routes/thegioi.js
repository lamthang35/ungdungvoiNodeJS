var express = require('express');
var router = express.Router();
var Baiviet = require('../models/baiviet');

/* GET home page. */
router.get('/', function(req, res, next) { 
  //var noidungs = Baiviet.find(); 
  Baiviet.find(function(err,docs){
    var baivietchuck = [];
    var chucksize = 3;
    for(var i=0; i<docs.length ; i+=chucksize){
      baivietchuck.push(docs.slice(i,i+chucksize));
    }

  res.render('thegioi', { title: 'Thế Giới', baiviets: baivietchuck });
  });
});

module.exports = router;
